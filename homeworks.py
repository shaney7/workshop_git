# 1 Sum of all digits in a number
def sum_of_digits(num):
    sum_result = 0

    while num % 10:
        digit = num % 10
        num = num // 10
        sum_result = sum_result + digit
    return sum_result
# print(sum_of_digits(8351))


# 2 Turn a number into a list of digits

def to_digits(num):
    my_list = []
    while num > 0:
        digit = num % 10
        my_list.append(digit)
        my_list.reverse()
        num = num // 10

    return my_list
# print(to_digits(8351))

# 3 Turn a list of digits into a number


def to_number(digits):
    num_sum = 0
    for digit in digits:
        if digit <= 10:
            num_sum = num_sum * 10
        else:
            num_sum = num_sum * 100
            num_sum = num_sum+digit
    return num_sum
# print(to_number([8,5,3,1]))

# 4 Fibonacci


def fib_number(num):
    current_num = 1
    previous_num = 0
    fib_num_arr = [1]
    fib_num_sum = 1

    for num in range(1, num):
        next_num = current_num+previous_num
        fib_num_arr.append(next_num)
        current_num = next_num
    for num in fib_num_arr:
        if num <= 10:
            fib_num_sum = fib_num_sum*10
        else:
            fib_num_sum = fib_num_sum*100
            fib_num_sum = fib_num_sum+previous_num
    return fib_num_sum
# print(fib_number(5))

# 5 Is number balanced


def is_number_balanced(number):
    number_digits = []
    left_part = 0
    right_part = 0
    while number % 10 > 0:
        digit = number % 10
        number_digits.append(digit)
        number = number // 10
        number_digits.reverse()
        list_lenght = len(number_digits)
        middle = len(number_digits)

    if len(number_digits) == 1:
        return True

    for index in range(list_lenght):
        if index < middle:
            left_part = left_part + number_digits[index]
        else:
            right_part = right_part + number_digits[index]
    return left_part == right_part
# print(is_number_balanced(9))
# print(is_number_balanced(8531))
# print(is_number_balanced(28471))


# 6 Decreasing sequence
def is_decreasing(mylist):
    for index in range(0, len(mylist) - 1):
        if mylist[index] <= mylist[index - 1]:
            return False
    return True
# print(is_decreasing([5,4,3,2,1]))
# print(is_decreasing([7,3,5,1,6]))


# 7 Sum of min. and max. number
def sum_of_min_and_max(arr):
    max_number = arr[0]
    min_number = arr[0]

    for digit in arr:
        if max_number < digit:
            max_number = digit
        if min_number > digit:
            max_number = digit
    return max_number+min_number
# print(sum_of_min_and_max([8,5,3,1]))


# 8 Sum all divisors of an integer
def sum_of_divisors(num):
    divisors_sum = 0
    for num in range(1, num + 1):
        if num % num == 0:
            num += divisors_sum
    return divisors_sum
# print(sum_of_divisors(6))
# print(sum_of_divisors(8))

# 9 Check if integer is prime


def is_prime(num):
    if num <= 1:
        return False
    for num in range(2, num):
        if num % num == 0:
            return False
    return True
# print(is_prime(1))
# print(is_prime(2))
# print(is_prime(6))
# print(is_prime(-24))

# 10 Palindrome


def is_it_palidrome(num):
    num_list = []
    number_list_reverse = []
    while num > 0:
        digit = num % 10
        num_list.append(digit)
        number_list_reverse.append(digit)
        num = num//10
        num_list.reverse()
    return num_list == number_list_reverse


# print(is_it_palidrome(1))
# print(is_it_palidrome(1337))
# print(is_it_palidrome(1551))
# print(is_it_palidrome(9))


# 11 Number containing a single digit
def contains_digit(number, digit):
    digits_list = []
    while number > 0:
        num_digit = number % 10
        digits_list.append(num_digit)
        number = number // 10
        digits_list.reverse()
        if digit in digits_list:
            return True
            return False

# print(contains_digit(23,2))
# print(contains_digit(8531,3))
# print(contains_digit(4244,1))
# print(contains_digit(1337,7))

# 12 Anagrams


def is_anagram(first_word, second_word):
    f_word = list(first_word)
    f_word.sort()
    s_word = list(second_word)
    s_word.sort()
    return (f_word == s_word)


#print(is_anagram("satan", "santa"))
#print(is_anagram("beach", "bleach"))
#print(is_anagram("listen", "silent"))


def count_substrings(haystack, needle):
    a_haystack = list(haystack)
    a_haystack.sort()
    a_needle = list(needle)
    a_needle.sort()
    count = haystack.count(needle)
    return (count)


#print(count_substrings("Programmers are elite", "elite"))
#print(count_substrings("Putting a random message just to check if its working", "what"))
#print(count_substrings("this isn't me this isn't you", "this"))

# Class Player and Attributes name, health
class Player(object):

    def __init__(self, name, health, weapons):
        self._name = name
        self._health = health
        self._weapons = weapons

    def shoot(self, Weapon, enemy_player):
        enemy_player.take_damage(fire)

    def take_damage(self, _damage):
        self._health -= self._damage

    def reload(self, Weapon):
        Weapon.reload()

    def get_name(self):
        return self._name

    def set_name(self, a):
        self._name = a

    def get_health(self):
        return self._health

    def set_health(self, b):
        self._health = b

    def get_weapons(self):
        return get_weapons

    def set_weapons(self, c):
        self._weapons = c


Player1 = Player("Never", 95, "Desert Eeagle")

print(Player1.get_name())
print(Player1.get_health())
print(Player1._weapons)


class Weapon(object):

    def __init__(self, _damage, _clip_size, _clip_bullets):
        self._damage = _damage
        self._clip_size = _clip_size
        self._clip_bullets = _clip_bullets

    def reload(self):
        self._clip_bullets = self._clip_size

    def fire(self):
        if self._clip_bullets > 0:
            self._clip_bullets -= 1
            return self.damage

        else:
            return 0


weap = Weapon(45, 7, 4)


print(weap._damage)
print(weap._clip_size)
print(weap._clip_bullets)


class ArmoredPlayer(Player):
    def __init__(self, name, health, weapons, armor):
        super().__init__(name, health, weapons)
        self.armor = armor
    def take_damage(self, damage):
        if self.armor => damage:
            self.armor -= damage:
        return damage:

        return super().take_damage(damage)
    
#
def slope_style_score(arr):
    min = arr[0]
    max = arr[0]
    scores = []

    for score in arr:
        if score > max:
            max = score
        if score < min:
            min = score
        if min == max:
            return round(min, 2)

        for idx in range(0, len(arr)):
            if arr[idx] != max:
                scores.append(arr[idx])

            if len(scores) == 0:    
                return round((min + max) / 2, 2)
            
            score = 0
            for idx in range(0, len(scores)):
                score = score + scores[idx]
            return round(score / len(scores), 2)

#print(slope_style_score([87,87,92,92,91]))

#
def what_is_my_sign(day, month):
    signs = [ "Aquaris", "Pisces", "Aries", "Taurus", "Gemini", "Cancer", "Leo", "Virgo", "Libra", "Scorpio", "Sagitarius","Capricorn"]
    if (month < 1):
        if month > 12:
            return "invalid month"
    if day < 1:
        if day > 31:
            return "invalid day"
    border = 19.5
    for idx in range(0,12):
        if idx == month - 1:
            if day < border:
                if month == 1:
                    return signs[11]
                else:
                    return signs[idx - 1]
            else:
                return signs[idx]
        if idx < 9:
            border = border + 0.5
        else:
            border = border - 1

#print(what_is_my_sign(20, 6))

#16
def count_coins(sum):
    coins = [1,0.5,0.2,0.1,0.05,0.02,0.01]
    dict = {100: 0,50: 0,20: 0,10: 0,5: 0,2: 0,1: 0}
    sum2 = 0.0
    for idx in range(0, len(coins)):
        while sum2 < sum:
            if sum2 + coins[idx] > sum:
                break
            else:
                sum2 = sum2 + coins[idx]
                dict[int(coins[idx] * 100)] = dict[int(coins[idx] * 100)] + 1
        
    return dict
#print(count_coins(0.50))



